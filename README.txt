#TEST socialH4ck
IMPORTANTE: en la raíz encontramos un .htaccess para evitar usar index, las rutas estan indicadas más abajo

##PARTE1
Para visualizar se nececita crear la DB MYSQL (en la raís de la carpeta part1 ubicamos  MYSQL_DB_STRUCTURE.sql)
* en manage/connect_mysqli.php esta la configuración de la bd
* Para probar cada request API: Desarrollé un client-accion.php dentro de la carpeta clients, sólo contiene curl para ejecutar la acción, rutas posibles:
	+ http://localhost/socialh4ck/part1/clients/client-list.php para consultar, este invoca al servicio que desarrollé y emplea las siguientes rutas: http://localhost/socialh4ck/users o http://localhost/socialh4ck/users/numero
	+ http://localhost/socialh4ck/part1/clients/client-delete.php para eliminar, internamente el servicio usa DELETE y usa http://localhost/socialh4ck/users/numero para indicar a quien eliminar
	+ http://localhost/socialh4ck/part1/clients/client-add.php invoca al servicio para agregar, el servicio que implementé usa  http://localhost/socialh4ck/users más lo enviado por POST
	+ http://localhost/socialh4ck/part1/clients/client-update.php para actualizar, similar a POST, salvo que uso PUT y resuelvo el formulario de llegada en el servicio que desarrollé para POST
	
	Todos los servicios usan index.php como controlador y los invoca de acuerdo a lo pedido
	Todos los servicios están en part1/add_user.php,part1/update_user.php,part1/list_user.php, part1/delete_user.php

##PARTE2
Para Visualizar la parte 2 hay que usar este enlace: http://localhost/socialh4ck/part2/
	Esta parte los colores se generan aleatoriamente.
	Consta de un HTML, y archivos javascript.
	Usado Es5 y prototype para la programación Javascript
	Otras librerías empleadas: Jquery, Bootstrap, Masonry