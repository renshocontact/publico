<?php
//se puede parametrizar esta parte, evitando tantos if anidados, usando freach y un metodo de validacion inteligente
$id=str_replace('/','',trim($url[1]) );
//metodo para tratar con los datos
parse_str(file_get_contents('php://input'), $values);
if(is_numeric($id) && $id>0 )
{
  if(isset($values['name']) && trim($values['name'])!='' && strlen(trim($values['name']))>=2)
  {
    if(isset($values['gender']) && trim($values['gender'])!='' && strlen(trim($values['gender']))>=8)
    {
      if(isset($values['company']) && trim($values['company'])!='' && strlen(trim($values['company']))>=2)
      {
        if(isset($values['email']) && trim($values['email'])!='' && strlen(trim($values['email']))>=3 && filter_var(trim($values['email']), FILTER_VALIDATE_EMAIL))
        {
          if(isset($values['telephone']) && trim($values['telephone'])!='' && strlen(trim($values['telephone']))>=10 )
          {
            if(isset($values['address']) && trim($values['address'])!='' && strlen(trim($values['address']))>=5)
            {
              include_once('manage/connect_mysqli.php');
              mysqli_set_charset($mysqli_connection,"utf8");
              $stmt = $mysqli_connection->prepare("UPDATE users SET name=?, gender=?, company=?, email=?, telephone=?, address=? where id=?;");
              $stmt->bind_param("ssssisd", $values['name'], $values['gender'], $values['company'],$values['email'],$values['telephone'],$values['address'],$id);
              $stmt->execute();
              $mysqli_connection->close();
              $response=json_encode(['success'=>['details'=>'Se ha actualizado el usuario '.$id]],JSON_UNESCAPED_UNICODE);
            }else {
              $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar la dirección']],JSON_UNESCAPED_UNICODE);
            }
          }else {
            $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el teléfono']],JSON_UNESCAPED_UNICODE);
          }
        }else {
          $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el correo electrónico}']],JSON_UNESCAPED_UNICODE);
        }
      }else {
        $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el nombre de la compañía']],JSON_UNESCAPED_UNICODE);
      }
    }else {
      $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el sexo del usuario']],JSON_UNESCAPED_UNICODE);
    }
  }else {
    $response=json_encode(['error'=>['input'=>'name','details'=>'Debe indicar el nombre del usuario, al menos dos caracteres']],JSON_UNESCAPED_UNICODE);
  }
}else {
  $response=json_encode(['error'=>['input'=>'id','details'=>'El id debe tener formato numerico y mayor a cero']],JSON_UNESCAPED_UNICODE);
}
echo $response;
exit;
?>
