<?php
$method = strtolower($_SERVER['REQUEST_METHOD']);
if( $method === 'post' && isset($_REQUEST['REQUEST_METHOD'])) {
    $tmp = strtolower((string)$_REQUEST['REQUEST_METHOD']);
    if( in_array( $tmp, array( 'put', 'delete', 'head', 'options' ))) {
        $method = $tmp;
    }
    unset($tmp);
}
if(isset($_GET['section']))
{
  $url=explode("/",$_GET['section']);
  if($method=='post' && isset($url[0]) && $url[0]=='users' && (!isset($url[1]) || trim($url[1])=='') )
  {
    //add
    include_once('add_user.php');
  }else {
    if($method=='delete' && isset($url[0]) && $url[0]=='users' && isset($url[1]) && trim($url[1])!='')
    {
      //delete
      include_once('delete_user.php');
    }else {
      if($method=='put' && isset($url[0]) && $url[0]=='users' && isset($url[1]) && trim($url[1])!='' )
      {
        //update
        include_once('update_user.php');
      }else {
        if($method=='get' && isset($url[0]) && $url[0]=='users')
        {
          //get
          include_once('list_user.php');
        }else {
          echo "acceso indebido";
          exit;
        }
      }
    }
  }
}
?>
