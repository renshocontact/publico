<?php
if(count($url)>1){
  $params=$url;
  unset($params[0]);
}else{
  $params=[];
}

$condition='';
if(isset($params[1]) && is_numeric(trim($params[1])) && trim($params[1])>0 )
{
  $condition=' WHERE id='.trim($params[1]);
}else {
  if(isset($params[1]) )
  {
    echo json_encode(['error'=>['input'=>'id','details'=>'El id debe  tener formato numerico y mayor a cero']]);
    exit;
  }
}
//no hay error, por lo tanto procedemos a la consulta
include_once('manage/connect_mysqli.php');
//no agregue limit por ser un ejemplo, tiene sus consideraciones en la url y aqui
$sql="SELECT 	id,name,gender,company,email,telephone,address FROM users ".$condition;
$mysqli_connection->set_charset("utf8");
$stmt = $mysqli_connection->prepare($sql.';');
$stmt->execute();
$stmt->store_result();
$bind=['id'=>'','name'=>'','gender'=>'','company'=>'','email'=>'','telephone'=>'','address'=>''];
$stmt->bind_result($bind['id'],$bind['name'],$bind['gender'],$bind['company'],$bind['email'],$bind['telephone'],$bind['address']);
$num_rows=$stmt->num_rows;
$response=[];
if($num_rows>0)
{
  for ($i=0;$i<$num_rows;++$i){
    $stmt->data_seek($i);
    $stmt->fetch();
    foreach ($bind as $key=>$value){
        $response[$i][$key]=$value;
    }
  }
}
$stmt->free_result();
$stmt->close();
$mysqli_connection->close();
echo json_encode($response,JSON_UNESCAPED_UNICODE);
unset($response);
exit;
?>
