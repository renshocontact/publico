<?php
$id=str_replace('/','',trim($url[1]) );
if(is_numeric($id) && $id>0 )
{
  include_once('manage/connect_mysqli.php');
  $stmt = $mysqli_connection->prepare("DELETE FROM users WHERE id=?;");
  $stmt->bind_param("d", $id);
  $stmt->execute();
  $mysqli_connection->close();
  $response=json_encode(['success'=>['details'=>'Se ha eliminado el usuario '.$id]],JSON_UNESCAPED_UNICODE);
}else {
  $response=json_encode(['error'=>['input'=>'id','details'=>'El id debe  tener formato numérico y mayor a cero']],JSON_UNESCAPED_UNICODE);
}
echo $response;
exit;
?>
