<?php
//se puede parametrizar esta parte, evitando tantos if anidados, usando freach y un metodo de validacion inteligente
$values=[];
if(isset($_POST['name']) && trim($_POST['name'])!='' && strlen(trim($_POST['name']))>=2)
{
  $values['name']=trim($_POST['name']);
  if(isset($_POST['gender']) && trim($_POST['gender'])!='' && strlen(trim($_POST['gender']))>=8)
  {
    $values['gender']=trim($_POST['gender']);
    if(isset($_POST['company']) && trim($_POST['company'])!='' && strlen(trim($_POST['company']))>=2)
    {
      $values['company']=trim($_POST['company']);
      if(isset($_POST['email']) && trim($_POST['email'])!='' && strlen(trim($_POST['email']))>=3 && filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL))
      {
        $values['email']=trim($_POST['email']);
        if(isset($_POST['telephone']) && trim($_POST['telephone'])!='' && strlen(trim($_POST['telephone']))>=10 )
        {
          $values['telephone']=trim($_POST['telephone']);
          if(isset($_POST['address']) && trim($_POST['address'])!='' && strlen(trim($_POST['address']))>=5)
          {
            $values['address']=trim($_POST['address']);
            include_once('manage/connect_mysqli.php');
            mysqli_set_charset($mysqli_connection,"utf8");
            $stmt = $mysqli_connection->prepare("INSERT INTO users (name, gender, company, email, telephone, address)VALUES(?,?,?,?,?,?);");
            $stmt->bind_param("ssssis", $values['name'], $values['gender'], $values['company'],$values['email'],$values['telephone'],$values['address']);
            $stmt->execute();
            $mysqli_connection->close();
            $response=json_encode(['success'=>['details'=>'Se ha insertado el nuevo usuario']],JSON_UNESCAPED_UNICODE);
          }else {
            $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar la dirección']],JSON_UNESCAPED_UNICODE);
          }
        }else {
          $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el teléfono']],JSON_UNESCAPED_UNICODE);
        }
      }else {
        $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el correo electrónico}']],JSON_UNESCAPED_UNICODE);
      }
    }else {
      $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el nombre de la compañía']],JSON_UNESCAPED_UNICODE);
    }
  }else {
    $response=json_encode(['error'=>['input'=>'gender','details'=>'Debe indicar el sexo del usuario']],JSON_UNESCAPED_UNICODE);
  }
}else {
  $response=json_encode(['error'=>['input'=>'name','details'=>'Debe indicar el nombre del usuario, al menos dos caracteres']],JSON_UNESCAPED_UNICODE);
}
echo $response;
exit;
?>
