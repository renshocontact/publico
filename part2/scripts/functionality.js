//NOTA: me basaré en ES5 y sus inconvenientes con el prototype
var Clients=(function(){
  var colors=['#87cdf9','#fcdeba','#F5F5BC','#FFFFE0','#b9e6c7','#cccc99','#deeebf'];
  var data={};
  function Clients(){}
  var coord=[];
  //funcion para obtener un elemento de la configuracion
  Clients.prototype.loadJson = function(elemento) {
    //para poder usar el this de loadJson y no confunda con el this local
    var _local = this;
    //problemas de seguridad no permiten conexiones remotas, la única es usar el json localmente
     $.getJSON( "scripts/data.json").
      done(function(json) {
        var temp={};
        $.each(json, function(index, client) {
         temp[index]={};
          $.each(client, function(label, value) {
            temp[index][label]=value;
          });
        });
        _local.data=temp;
        //comenzamos a armar los datos
        _local.makeClients();
      }).
      fail(function() {
        console.log( "error" );
      });
  }
//funcion que pinta todo lo necesario en pantalla
  Clients.prototype.makeClients = function() {
    var html={'html_active_clients':'','html_inactive_clients':''}
    var _local = this;
    //vamos a recorrer los datos e ir trabajando los requerimientos en simultáneo
    //aqui recorremos cliente por cliente
    $.each(this.data, function(index, client) {
      //valor al azar para obtener el color correspondiente
      var color=_local.getRandomArbitrary(0,6);
      var current_client='';
      if(client['isActive']==true)
      {
        //activos
        current_client='html_active_clients';
      }else {
        //inactivos
        current_client='html_inactive_clients';
      }
      //agregamor un clear para que no se aniden las cada de varios clientes a la vez
      if(html[current_client]!='')
        html[current_client]=html[current_client]+'<div style="clear:both;"></div>';
      html[current_client]=html[current_client]+'<div class="current-client row">';
        //recorremos todas las columnas del cliente
      var about='';

      $.each(client, function(key, value) {
        //ponemos datos, estilos...
        //evitamos por ahora a about, al tener mucho contenido conviene que sea el último, por eso lo guardamos en una variable
        if(key!='about')
        {
          var temp_content='';
          if(key=='friends')
          {
            if(value.length>0)
            {
              //separamos los amigos que son un objeto
              temp_content='<ul>';
              $.each(value, function(split_key, split_value) {
                temp_content=temp_content+'<li>'+split_value['name']+'</li>';
              });
              temp_content=temp_content+'</ul>';
            }
          }else {
            //la imahen la ponemos en un img
            if(key=='picture')
            {
              temp_content='<div style="min-height:120px;"><img src="'+value+'"/></div>';
            }else {
                temp_content=value;
            }
          }
          //ni de broma hacemos un cuadro coin longitud y latitud, no tiene sentido para eso está el mapa
          if(key!='latitude' && key!='longitude')
            html[current_client]=html[current_client]+'<div class="col-lg-2 col-md-3 col-sm-5 col-xs-5 sections" style="background-color:'+colors[color]+'"><div class="title-medium">'+key.capitalize()+'</div><div class="data">'+temp_content+'</div></div>';
        }else{
          about='<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 sections" style="background-color:'+colors[color]+';height:auto;margin-left:4px"><div class="title-medium">'+key.capitalize()+'</div><div class="data">'+value+'</div></div>';;
        }
        //si encontramos las coordenadas, las guardamos para usarlas en el mapa
        if(key!='latitude' || key!='longitude')
        {
          if(coord[index]==null)
            coord[index]={'name':value['name']};
          coord[index][key]=value;
        }

      });
      //pintamos about, de existir
      if(about!='')
        html[current_client]=html[current_client]+'<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 final" style="display:inline-block">'+about;
      html[current_client]=html[current_client]+'<div class="col-lg-6 col-md-5 col-sm-12 col-xs-12 map-clients sections" style="background-color:'+colors[color]+'"><div class="title-medium">Location</div><div class="data" id="map-id-'+index+'"></div></div></div><div class="clearfix"><p>&nbsp;</p></div></div>';
    });
    //todo lo armado lo ponemos en sus divs respectivos para su renderización}
    $("#actives-clients .data-container").append(html['html_active_clients']);
    $("#inactives-clients .data-container").append(html['html_inactive_clients']);
    //efecto  de cuadros
    $("#actives-clients .data-container  .current-client").masonry();
    $("#inactives-clients .data-container  .current-client").masonry();
    //ahora los mapas...
    _local.generateMap();
  }
  //esto esta mal, realmente deberia estar dentro de otra "clase" como Math o una clase desarrollada para numeros, para el test la pongo en la clase creada
  Clients.prototype.getRandomArbitrary=function(min, max) {
    return Math.round((Math.random() * (max - min) + min));
  }
  Clients.prototype.generateMap=function() {
    //recorremos todas las coordenadas, ponemso el marker y un popup
    $.each(coord, function(key, value) {
     var latLng = new google.maps.LatLng(value['latitude'], value['longitude']);
     var mapOptions = {
       zoom: 8,
       center: latLng
       //mapTypeId: google.maps.MapTypeId.SATELLITE
     };
     var map = new google.maps.Map(document.getElementById('map-id-'+key), mapOptions);
     var marker = new google.maps.Marker({
      position: latLng,
      map: map,
      title: value['name'].capitalize(),
      label: value['name'].capitalize()
     });
     var info = new google.maps.InfoWindow({
        content: 'Coordenadas de '+value['name'].capitalize()+': <b>'+value['latitude']+', '+value['longitude']+'</b>'});
        info.open(map, marker);
   });
  }

  return Clients;
})();

//funcion utility para capitalizar
String.prototype.capitalize= function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
function initMaps()
{
  var map = new google.maps.Map() ;
  map.setTilt(45);
}
//aqui inicia todo
window.onload=function(){
  $(document).ready(function(){
    test= new Clients();
    test.loadJson();
  });
}
